<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unical Complaint Management System</title>

<?php  echo link_tag('assets/css/board.css'); ?>


</head> 
<body>
	<div id="wrapper">
    	<div id="header">
        	
            <div id="creat" style="width:1000px; margin-left:auto; margin-right:auto">
            	<img src="<?php echo base_url()?>assets/images/logo.jpg" width="50px" height="45px" style="float:left; margin-right:20px;
                 margin-top:10px"/>
                  <?php echo anchor("Mainboard/logout", "Logout");?>
                
                <h4>Unical Complaint Management System</h4>
               
            </div>    
           
        </div><!--end header-->
        
        <div id="wrapper2">
            <div id="component">
            
                <div id="profile">
                    <img src="<?php echo $userinfo->Passport;?>" id="img1"/><br />
                    <p style=" font-weight:bold"><?php echo ucfirst($userinfo->FirstName)?>&nbsp; <?php echo ucfirst($userinfo->LastName)?></p>
                 <img src="<?php echo base_url()?>assets/images/camera.png" width="30px" height="30px" style="float:right; margin-right:20px; cursor:pointer" onclick="bringuploader()" title="Change profile photo" />
                </div><!--ends profile-->
                
                <div id="middle">
                
                    <div id="form1">
                        <div id="res">Maximum of 400 characters</div>
                        
                        <textarea maxlength="400" placeholder="Logde a Complain" id="com" ></textarea>
                        <br />
                        
                        <input type="text" placeholder="Complain Cause (Staff, Student, Group of people)" id="textcomp" />
                         <div id="recievers">
                        </div>
                        <input type="submit" value="Submit"  onclick="logcomplain()"/><br /><br />
                    </div>
                  
                    
                     <div id="form9">
                     	
                     </div>
                    
                
                </div><!--ends middle-->
                
                <div id="announcement">
                    <h4>User's Profile</h4>
                    <hr />
                    <?php if($userinfo->UserType == "student"){?>
                    <div class="holder">Account Type</div>  <div class="info">Student</div> <div class="cl"></div>
                    <div class="holder">Matric No</div>  <div class="info"><?php echo $userinfo->MatricNo;?></div> <div class="cl"></div>
                    <div class="holder">First Name</div>  <div class="info"><?php echo $userinfo->FirstName;?></div> <div class="cl"></div>
                    <div class="holder">Last Name</div>  <div class="info"><?php echo $userinfo->LastName;?></div> <div class="cl"></div>
                    <div class="holder">Gender</div>  <div class="info"><?php echo $userinfo->Gender;?></div> <div class="cl"></div>
                    <hr />
                     <div class="holder">Phone</div>  <div class="info"><?php echo $userinfo->Phone;?></div> <div class="cl"></div>
                    <div class="holder">Department</div>  <div class="info"><?php echo $userinfo->Department;?></div> <div class="cl"></div>
                    <div class="holder">Faculty</div>  <div class="info"><?php echo $userinfo->Faculty;?></div> <div class="cl"></div>
                    <div class="holder">Last Login</div>  <div class="info"><?php echo $userinfo->LastLogin;?></div> <div class="cl"></div>
                     <div class="holder">Email</div>  <div class="info"><?php echo $userinfo->Email;?></div> <div class="cl"></div>
                    <?php } else{?>
                    
                    <div class="holder">Account Type</div>  <div class="info">Staff</div> <div class="cl"></div>
                    <div class="holder">Title</div>  <div class="info"><?php echo $userinfo->Title;?></div> <div class="cl"></div>
                    <div class="holder">First Name</div>  <div class="info"><?php echo $userinfo->FirstName;?></div> <div class="cl"></div>
                    <div class="holder">Last Name</div>  <div class="info"><?php echo $userinfo->LastName;?></div> <div class="cl"></div>
                    <div class="holder">Gender</div>  <div class="info"><?php echo $userinfo->Gender;?></div> <div class="cl"></div>
                    <hr />
                     <div class="holder">Phone</div>  <div class="info"><?php echo $userinfo->Phone;?></div> <div class="cl"></div>
                    <div class="holder">Post</div>  <div class="info"><?php echo $userinfo->Position;?></div> <div class="cl"></div>
                    <div class="holder">Last Login</div>  <div class="info"><?php echo $userinfo->LastLogin;?></div> <div class="cl"></div>
                     <div class="holder">Email</div>  <div class="info"><?php echo $userinfo->Email;?></div> <div class="cl"></div>
                    
                    <?php }?>
                    
                </div><!--ends--announcement-->
                
                <div class="cl"></div>
                
                
            </div><!--ends div component-->
        </div><!--end wrapper2-->
        

    
	</div><!--ends wrapper-->
</body>
</html>
<div id="uploader">
	<h1>Change your Profile Photo</h1>
    <div id="pix"><img src="<?php echo $userinfo->Passport;?>" id="img2"/> </div>
    <input type="file" id="upl" / onchange="doupload()" ><br />
    <button style="float:right; margin-right:10px;  width:100px; height:50px" onclick="closeup()">Cancel</button>
    
    <button style="float:left; margin-left:10px; display:none; width:100px; height:50px" onclick="finish()"  id="finish">Finish</button>
    <div id="error"></div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/board.js');?>"></script>

