<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unical Complaint Management System</title>

<?php  echo link_tag('assets/css/one.css'); ?>


</head> 

<body style="background-image:url(<?php echo base_url()?>assets/images/one.jpg); background-repeat:no-repeat; background-size:cover">

    <div id="mother">
    
    	<div id="linker">
        	<div class="lin"><?php  echo anchor('home/register', 'SignUp');?></div>
        	 <div class="lin"><a href="javascript:void(0)" onclick="showadminlogin()">AdminLogin</a></div>
        	<div class="lin"><a href="javascript:void(0)" onclick="showstafflogin()">StaffLogin</a></div>
            <div class="lin"><a href="javascript:void(0)" onclick="showstudentlogin()">StudentLogin</a></div>
           
        </div><!--ends linker-->
        
        <div id="stand">
        
        	<div id="intro">
            <br />
            	<h1 class="int">University of Calabar<br />Complaint Management System </h1>
            </div>
            
        	<div id="login">
            
            <div id="studentlogin">
            		<br />
                    <h3 style="margin-left:20px; color:#1c4174">Student Login</h3><br />
                    <input type="text" placeholder="Matric Number"  id="matric" /><br />
                    
                    <input type="password" placeholder="Password" id="password"  /><br />
                    
                    <input type="submit" value="Login" onclick="loginstudent()"  /><br />
                    
                    <div id="error" style="margin-left:20px; color:blue"></div>
                </div><!--ends studentlogin-->  
                
            	<div id="adminlogin" style="display:none">
            		<br />
                    <h2 style="margin-left:20px; color:#9d4c4d">Admin Login</h2><br />
                    <input type="text" placeholder="Admin Username"  id="adminusername" /><br />
                    
                    <input type="password" placeholder="Password" id="adminpassword"  /><br />
                    
                    <input type="submit" value="Login"  onclick="loginadmin()" /><br />
                     <div id="error3" style="margin-left:20px; color:blue"></div>
                </div><!--ends studentlogin-->   
                
                
                
                
                <div id="stafflogin" style="display:none">
            		<br />
                    <h2 style="margin-left:20px; color:#9d4cd4">Staff Login</h2><br />
                    <input type="text" placeholder="Email"  id="staffusername" /><br />
                    
                    <input type="password" placeholder="Password" id="staffpassword"  /><br />
                    
                    <input type="submit" value="Login" onclick="loginstaff()" /><br />
                     <div id="error2" style="margin-left:20px; color:blue"></div>
                </div><!--ends studentlogin-->  
                 
            </div><!--ends login-->
        </div><!--ends stand-->
        
    </div><!--end mother-->

</body>
</html>
<script type="text/javascript" src="<?php echo base_url('assets/js/homescript.js');?>"></script>
