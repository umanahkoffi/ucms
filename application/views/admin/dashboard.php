<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Unical Complaint Management System</title>
<?php  echo link_tag('assets/css/admin.css'); ?>
</head> 
<body">
	<div id="header">
    	<div id="holdhead"><h3>Admin Dashboard</h3></div>
    </div>
    
    <div id="leftmenu">
    
    	<div class="sub"><a href="javascript:void(0)" onclick="fetchunread()">Unresolved Complains</a></div>
    	<div class="sub"><a href="javascript:void(0)" onclick="fetchread()">Resolved Complains</a></div>
        <div class="sub"><a href="javascript:void(0)" onclick="displaysearchform()">Search Complains</a></div>
       <div class="sub"><?php echo anchor("Admin/logout", "Logout")?></div>
    </div>
    
    
    <div id="right" style="background-image:url(<?php echo base_url()?>assets/images/paper.png)">
    	<div id="actionzone">
        
        </div>
    </div>
    
    <div class="cl">
    </div>

    
</body>
</html>
<script type="text/javascript" src="<?php echo base_url('assets/js/admin.js');?>"></script>
