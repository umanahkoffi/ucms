<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unical Complaint Management System</title>

<?php  echo link_tag('assets/css/register.css'); ?>


</head> 

<body>

    <div id="mother">
    
    	<div id="linker">
        	
            <div class="lin"><a href="javascript:void(0)" onclick="showstafflogin()">Staff Registration</a></div>
            <div class="lin"><a href="javascript:void(0)" onclick="showstudentlogin()">Student Registration</a></div>
        	<div class="lin"><?php  echo anchor('home', 'Welcome Page');?></div>
            
        </div><!--ends linker-->
        
        <div id="stand">
        
        	<div id="intro">
            <br />
            	<h1 class="int">University of Calabar<br />Complaint Management System</h1><br />
                <h3 class="int2">Please Take time to provide accurate information about yourself. Note that providing fraudulent information is a punishable offence </h3>
                
                <br />
                <div id="image">
                	<img src="<?php echo base_url()?>assets/images/two.jpg" />
                </div>
                
                
            </div><!--ends --stand-->
            
        	<div id="login">
            
            <div id="studentlogin">
            		<br />
                    <h3 style="margin-left:20px; color:#1c4174">Student Registration console</h3><br />
                    <input type="text" placeholder="Matric Number"  id="matric" /><br />
                    
                    <input type="text" placeholder="First Name"  id="firstname" /><br />
                      <input type="text" placeholder="Last Name"  id="lastname" /><br />
                       <input type="text" placeholder="Phone Number"  id="phone" /><br />
                        <input type="text" placeholder="email"  id="email" /><br />
                       <input type="text" placeholder="Faculty"  id="faculty" /><br />
                       <input type="text" placeholder="Department"  id="department" /><br />
                       <select id="gender">
                       <option>Gender</option>
                       <option>Male</option>
                       <option>Female</option>
                        </select>
                    <input type="password" placeholder="Password" id="password"  />
                    <div id="error" style="margin-left:20px"></div>
                    <input type="submit" value="Register" onclick="regstudent()"  /><br />
                </div><!--ends studentlogin-->  
                
                 <div id="stafflogin" style="display:none">
            		<br />
                    <h3 style="margin-left:20px; color:#1c4174">Staff Registration console</h3><br />
                 <select id="title">
                	<option>Title</option>
                    
                       <option>Prof</option>
                       <option>Dr</option>
                       <option>Mr</option>
                       <option>Mrs</option>
                       <option>Master</option>
                       <option>Miss</option>
                        </select>
                     
                     
                      <select id="position" onchange="checkothers(this.value)">
                	<option value="">Position Held</option>
                     <option>Vice Chancellor</option>
                      <option>Deputy Vice Chancellor</option>
                      <option>Registrar</option>
                      <option>Bursar</option>
                      <option>Accountant</option>
                      <option>Librarian</option>
                       <option>Dean</option>
                        <option>HOD</option>
                       <option>Lecturer</option>
                       <option>Lab Attendant</option>
                       <option>Technologist</option>
                       <option>Registry staff</option>
                       <option>Works Staff</option>
                       <option>Cleaner</option>
                       <option>Others</option>
                        </select>
                         <input type="text" placeholder="For Others please specify"  id="position" class="others" style="display:none" /><br />
                        
                    <input type="text" placeholder="First Name"  id="firstname2" /><br />
                    
                      <input type="text" placeholder="Last Name"  id="lastname2" /><br />
                      
                       <input type="text" placeholder="Phone Number"  id="phone2" /><br />
                       
                        <input type="text" placeholder="email"  id="email2" /><br />
                       
                       <select id="gender2">
                       <option>Gender</option>
                       <option>Male</option>
                       <option>Female</option>
                        </select>
                    <input type="password" placeholder="Password" id="password2"  />
                    <div id="error2" style="margin-left:20px"></div>
                    
                    
                    <input type="submit" value="Register" onclick="regstaff()"  /><br />
                </div><!--ends studentlogin--> 
                
            	
            </div><!--ends login-->
        </div><!--ends stand-->
        
    </div><!--end mother-->

</body>
</html>
<script type="text/javascript" src="<?php echo base_url('assets/js/signupstudent.js');?>"></script>


<script type="text/javascript">


function showstafflogin()
{
	document.getElementById("stafflogin").style.display = "block";
	document.getElementById("studentlogin").style.display = "none";
	
}
function showstudentlogin()
{
	document.getElementById("stafflogin").style.display = "none";
	document.getElementById("studentlogin").style.display = "block";
	
}
</script>