<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainboard extends CI_Controller {
	
	
	public function __Construct(){
	  parent::__Construct ();
	  $this->load->database(); // load database
	  $this->load->helper('url');
	  $this->load->helper('html');
	  $this->load->library('session');
	  $this->load->library('upload');
 
	 }

	 
	public function user($fullname)
	{
		if(!isset($_SESSION["userid"])){
			redirect('home/index');
		}
		
		$this->load->helper('url');
		$this->load->helper('html');
		$sql = "select * from users where UserId = ?";
		$query = $this->db->query($sql, array($_SESSION["userid"]));
		$query = $query->row();
		if($query->Passport == NULL){
			$query->Passport =base_url()."assets/images/avatar.jpg";
		}
		else{
			$query->Passport =base_url()."uploads/".$query->Passport;
		}
		$data['userinfo'] = $query;
		$this->load->view('user/welcome', $data);
		
	}
	
	public function processupload()
	{
		$config['upload_path'] = 'uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name']  = $_SESSION["userid"];
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('data'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        print_r($error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
						$_SESSION["filename"] = $data["upload_data"]['file_name'];
						print_r(json_encode(array("status"=>"uploaded", "filename"=>$data["upload_data"]['file_name'])));
                        
                }

	}
	
	public function updatepassport()
	{
		$sql = "update  users  set Passport = ? where UserId = ?";
		if($this->db->query($sql, array($_SESSION["filename"], $_SESSION["userid"]))){
			echo "100";
		}
		else{
			echo "300";
		}
	}
	
	
	public function logout()
	{
		$lastlogin = date("d-M-Y:h-i");
		$sql = "update  users  set LastLogin = ? where UserId = ?";
		if($this->db->query($sql, array($lastlogin, $_SESSION["userid"]))){
			unset($_SESSION["userid"]);
			 redirect('home');
		}
		
	}
	
	public function searchcomplaincause()
	{
		$str = $this->input->post("data");
		$data = json_decode($str);
		if($data->str !== ""){
			$keyword = $data->str;
			$this->db->select('*');
			$this->db->from('users');
			$this->db->like('FirstName', $keyword, 'after');//options are before, after, both
			$this->db->or_like('LastName', $keyword, 'after');
			$result =  $this->db->get()->result_array();
			$dat["res"] = $result;
			$this->load->view('user/resdata', $dat);
			
			
		
		}//ends first if
		
	}//ends function
	
	
	public function setvaluetocomplain()
	{
		$str = $this->input->post("data");
		$data = json_decode($str);
		$sql = "select * from users where UserId = ?";
		$query = $this->db->query($sql, array($data->str));
		$query = $query->row();
		$statement = $query->FirstName. " ".$query->LastName. " "."a". " ".$query->UserType. " ". "with phone number". " ".$query->Phone;
		echo $statement;
	}
	
	
	public function logcomplain()
	{
		$str = $this->input->post("data");
		$data = json_decode($str);
		
		$dateLog = date("d-M-Y");
		
		
		$sql = "insert into complain (DateLogged, Cause, MainBody,  UserId) values(?, ?, ?, ?)";
		$this->db->query($sql, array($dateLog, $data->cause, $data->reason, $_SESSION["userid"] ));
		
		$sql = "Select * from Complain where UserId = ? order by ComplainId desc";
		$result = $this->db->query($sql, array($_SESSION["userid"]));
		$data = $result->result();
		$d["render"] = $data; 
		$this->load->view("user/rendercomplain", $d);
		
	}
	
	public function loadpreviouscomplaints()
	{
		$sql = "Select * from Complain where UserId = ? order by ComplainId desc";
		$result = $this->db->query($sql, array($_SESSION["userid"]));
		$data = $result->result();
		$d["render"] = $data; 
		$this->load->view("user/rendercomplain", $d);
	}
	
}
