<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	
	public function __Construct(){
	  parent::__Construct ();
	  $this->load->database(); // load database
	  $this->load->helper('url');
	  $this->load->helper('html');
	  $this->load->library('session');
	  $this->load->library('upload');
 
 
	 }

	 
	
	
		public function dashboard()
	{
		if(!isset($_SESSION["admincomplain"])){
			redirect('home/index');
		}
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->view('admin/dashboard');
	}
	
	public function logout()
	{
		unset($_SESSION["admincomplain"]);
		
			redirect('home/index');
	}
	
	
	
	public function staffregister()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->password);
		$lastlogin = date("d-M-Y:h-i");
		$sql = "insert into users (Title, FirstName, LastName, Phone, Email, Position, Password,  LastLogin, 
		UserType, Gender) values(?,?,?,?,?,?,?,?,?, ?)";
		
		if($this->db->query($sql, array($data->title, $data->firstname, $data->lastname, $data->phone, $data->email, $data->position, $password,
		 $lastlogin, 'staff', $data->gender)))
		 {
			$_SESSION["userid"]  = $this->db->insert_id();
			echo 100; 
		 }
	}
	
	public function fetchunresolvedcomplains($id)
	{
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchunresolvedcomplains($id);
		$this->load->view("admin/unresolved", $f);
	}
	
	public function fetchonecomplain()
	{
		
		$json = $this->input->post('data');
		$d = json_decode($json);
		$id = $d->id;
		
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchonecomplain($id);
		
		$data["details"] = $f;
		$this->load->view("admin/onecomplain", $data);
		
	}
	
	
	public function fetchonecomplain2()
	{
		
		$json = $this->input->post('data');
		$d = json_decode($json);
		$id = $d->id;
		
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchonecomplain($id);
		
		$data["details"] = $f;
		$this->load->view("admin/onecomplain2", $data);
		
	}
	
	public function viewdetailsresolved()
	{
		$json = $this->input->post('data');
		$d = json_decode($json);
		$id = $d->id;
		
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchonecomplain($id);
		
		$data["details"] = $f;
		$this->load->view("admin/onecomplainresolved", $data);
	}
	
	public function respond()
	{
		$json = $this->input->post('data');
		$d = json_decode($json);
		$date = date("d-M-Y");
		$sd = "update complain set Solution = ?, TreatedDate = ?, Status = 1 where ComplainId = ?";
		$this->db->query($sd, array($d->res, $date, $d->id));
		
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchunresolvedcomplains(0);
		$this->load->view("admin/unresolved", $f);
	}
	
	
	public function fetchresolvedcomplains($id)
	{
		$this->load->model('Admin_model');
		$f = $this->Admin_model->fetchresolvedcomplains($id);
		$this->load->view("admin/resolved", $f);
	}
	
	public function displaysearchform()
	{
		
		$this->load->view("admin/searchform");
	}
	
	
	public function dosearch()
	{
		$json = $this->input->post('data');
		$id = json_decode($json);
		$id = $id->valve;
		if($id == ""){
		die;	
		}
		
		$this->load->model('Admin_model');
		$f = $this->Admin_model->searchcomplain($id);
		$this->load->view("admin/dosearch", $f);
	}
	
	public function respond2()
	{
		$json = $this->input->post('data');
		$d = json_decode($json);
		$date = date("d-M-Y");
		$sd = "update complain set Solution = ?, TreatedDate = ?, Status = 1 where ComplainId = ?";
		$this->db->query($sd, array($d->res, $date, $d->id));
		
		$this->load->view("admin/done");
	}
	
	
}
