<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	
	public function __Construct(){
	  parent::__Construct ();
	  $this->load->database(); // load database
	  $this->load->library('session');
 
	 }

	 
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->view('home');
	}
	
	public function comment($rand)
	{
		echo $rand;
	}
	
	
	public function register()
	{
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->view('register');
	}
	
	public function processregister()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->password);
		
		$count = "select *   from users where MatricNo = ?";
		$result = $this->db->query($count, array($data->matricno));
		if($result->num_rows() > 0){
			echo "121";
		}
		else{
		$lastlogin = date("d-M-Y:h-i");
		$sql = "insert into users (MatricNo, FirstName, LastName, Phone, Email, Faculty, Department, Password,  LastLogin, 
		UserType, Gender) values(?,?,?,?,?,?,?,?,?, ?, ?)";
		
		if($this->db->query($sql, array($data->matricno, $data->firstname, $data->lastname, $data->phone, $data->email, $data->faculty,
		 $data->department, $password, $lastlogin, 'student', $data->gender)))
		 {
			 $_SESSION["userid"]  = $this->db->insert_id();
			echo 100; 
		 }
		}
	}
	
	
	public function staffregister()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->password);
		$lastlogin = date("d-M-Y:h-i");
		$sql = "insert into users (Title, FirstName, LastName, Phone, Email, Position, Password,  LastLogin, 
		UserType, Gender) values(?,?,?,?,?,?,?,?,?, ?)";
		
		if($this->db->query($sql, array($data->title, $data->firstname, $data->lastname, $data->phone, $data->email, $data->position, $password,
		 $lastlogin, 'staff', $data->gender)))
		 {
			$_SESSION["userid"]  = $this->db->insert_id();
			echo 100; 
		 }
	}
	
	public function processloginstudent()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->password);
		
		$sql = "select *   from users where MatricNo = ? and Password = ?";
		$result = $this->db->query($sql, array($data->matricno, $password));
		if($result->num_rows() == 1){
			$userData = $result->row();
			$_SESSION["userid"] = $userData->UserId;
			
			$newArray = array("status" =>"registerduser", "names"=>$userData->FirstName ."-".$userData->LastName);
			$newArray = json_encode($newArray);
			echo $newArray;
			
		}
		else{
			echo 44;
		}
		
		
	}
	
	
	public function processloginstaff()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->staffpassword);
		
		$sql = "select *   from users where Email = ? and Password = ? and UserType = 'staff' ";
		$result = $this->db->query($sql, array($data->staffusername, $password));
		if($result->num_rows() == 1){
			$userData = $result->row();
			$_SESSION["userid"] = $userData->UserId;
			$newArray = array("status" =>"registerduser", "names"=>$userData->FirstName ."-".$userData->LastName);
			$newArray = json_encode($newArray);
			echo $newArray;
			
		}
		else{
			echo 44;
		}
		
		
	}
	
	
	public function loginadmin()
	{
		$json = $this->input->post('data');
		$data = json_decode($json);
		$password = base64_encode($data->adminpassword);
		
		
		$sql = "select *   from admin where Username = ? and Password = ?  ";
		$result = $this->db->query($sql, array($data->adminusername, $password));
		if($result->num_rows() == 1){
			$userData = $result->row();
			$_SESSION["admincomplain"] = "admincomplain";
			
			echo 1003;
			
		}
		else{
			echo 44;
		}
		
		
	}
}
