<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	
	public function __Construct(){
	  parent::__Construct ();
	  $this->load->database(); // load database
	  //$this->load->library('session');
	   }

	 
	
	
	
	
	public function fetchunresolvedcomplains($id)
	{
		$sql = "select * from complain where ComplainId > ?  and Status = ? order by  ComplainId desc Limit 10";
		$res = $this->db->query($sql, array( $id, 0));
		$res = $res->result();
		
		foreach($res as $one){
			$sql2 = "select * from users where UserId = ? ";
			$sqlite = $this->db->query($sql2, array($one->UserId));
			$newResult = $sqlite->row();
			$one->FirstName = $newResult->FirstName;
			$one->LastName = $newResult->LastName;
			
			$one->UserType = $newResult->UserType;
			
		}
		
		$f["info"] = $res;
		return $f;
	}
	
	public function fetchonecomplain($id)
	{
		
		
		$sql = "select * from complain where ComplainId = ?";
		$res = $this->db->query($sql, array($id));
		$res = $res->row_array();
		
		$sql2 = "select * from users where UserId = ? ";
		
		$userdetails = $this->db->query($sql2, array($res["UserId"]));
		$newResult = $userdetails->row();
		$res["FirstName"] = $newResult->FirstName;
		$res["LastName"] = $newResult->LastName;
		$res["Phone"] = $newResult->Phone;
			
		
		return  $res;
		
	}
	
	
	public function fetchresolvedcomplains($id)
	{
		
		$sql = "select * from complain where ComplainId > ?  and Status = ? order by  ComplainId desc Limit 10";
		$res = $this->db->query($sql, array( $id, 1));
		$res = $res->result();
		
		foreach($res as $one){
			$sql2 = "select * from users where UserId = ? ";
			$sqlite = $this->db->query($sql2, array($one->UserId));
			$newResult = $sqlite->row();
			$one->FirstName = $newResult->FirstName;
			$one->LastName = $newResult->LastName;
			
			$one->UserType = $newResult->UserType;
			
		}
		
		$f["info"] = $res;
		return $f;
	}
	
	public function searchcomplain($id)
	{
		$this->db->select('*');
			$this->db->from('complain');
			$this->db->like('MainBody', $id, 'both');//options are before, after, both
			$this->db->or_like('DateLogged', $id, 'after');
			$res=  $this->db->get()->result();
			
			
		
		
		foreach($res as $one){
			$sql2 = "select * from users where UserId = ? ";
			$sqlite = $this->db->query($sql2, array($one->UserId));
			$newResult = $sqlite->row();
			$one->FirstName = $newResult->FirstName;
			$one->LastName = $newResult->LastName;
			
			$one->UserType = $newResult->UserType;
			
		}
		
		$f["info"] = $res;
		return $f;
	}
	
}
