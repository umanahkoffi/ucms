
var base_url = "http://localhost/ucms/";

if(window.XMLHttpRequest){
	var xml = new XMLHttpRequest();
	
}
else{
	var xml = new ActiveXObject("Microsoft.XMLHttp");
	
}



function regstudent()
{
	var matricno = document.getElementById("matric").value;
	var firstname = document.getElementById("firstname").value;
	var lastname = document.getElementById("lastname").value;
	var phone = document.getElementById("phone").value;
	var email = document.getElementById("email").value;
	var faculty = document.getElementById("faculty").value;
	var department = document.getElementById("department").value;
	var password = document.getElementById("password").value;
	var gender = document.getElementById("gender").value;
	
	if(matricno == ""){
		warning("Provide your Matric Number");
		return
	}
	if(firstname == ""){
		warning("Provide your First name");
		return;
	}
	if(lastname == ""){
		warning("Provide your last name");
		return;
	}
	if(phone == ""){
		warning("Provide your Phone number");
		return;
	}
	if(email == ""){
		warning("Provide your email");
		return
	}
	if(faculty == ""){
		warning("Provide your faculty");
		return
	}
	if(department == ""){
		warning("Provide your department");
		return
	}
	
	if(password == ""){
		warning("Create a password");
		return
	}
	
	if(gender == "Gender"){
		warning("Chose Gender");
		return
	}
	var url = base_url + "home/processregister";
	xml.open("POST", url, true)
		var datas = JSON.stringify({"firstname":firstname,  "lastname":lastname, "phone":phone, "email":email,
		"faculty":faculty, "department":department, "password":password, "matricno":matricno, "gender":gender});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			if( xml.responseText == 100){
				window.location = base_url + "mainboard/user/" + firstname + "-" + lastname;
			}
			if( xml.responseText == 121){
				warning("This Number has been registed before");
			}			
			
		}
	}
	xml.send(dats);		
}

function warning(note)
{
	document.getElementById("error").innerHTML = note;
	document.getElementById("error").style.color = "red";
}
function warning2(note)
{
	document.getElementById("error2").innerHTML = note;
	document.getElementById("error2").style.color = "red";
}

function checkothers(value)
{
	if(value == "Others"){
		document.getElementsByClassName("others").item(0).style.display = "block";
		document.getElementsByClassName("others").item(0).style.border = "1px solid Red";
	}
	else{
		document.getElementsByClassName("others").item(0).style.display = "none";
	}
}


function regstaff()
{
	var title = document.getElementById("title").value;
	var position = document.getElementById("position").value;
	
	var firstname = document.getElementById("firstname2").value;
	
	var lastname = document.getElementById("lastname2").value;
	
	var phone = document.getElementById("phone2").value;
	var email = document.getElementById("email2").value;
	var password = document.getElementById("password2").value;
	var gender = document.getElementById("gender2").value;
	
	if(title == "Title"){
		warning2("Provide your Title");
		return
	}
	
	if(position == ""){
		warning2("Provide your Position");
		return
	}
	
	if(position == "Others"){
		
		position = document.getElementsByClassName("others").item(0).value
			if(position == ""){
				warning2("if poition is Others please specify");
				return
			}
		
	}
	if(firstname == ""){
		warning2("Provide your First name");
		return;
	}
	
	if(phone == ""){
		warning2("Provide your Phone number");
		return;
	}
	
	if(lastname == ""){
		warning2("Provide your Last Name");
		return;
	}
	
	if(email == ""){
		warning2("Provide your email");
		return
	}
	
	
	if(password == ""){
		warning2("Create a password");
		return
	}
	
	if(gender == "Gender"){
		warning2("Chose Gender");
		return
	}
	var url = base_url + "home/staffregister";
	xml.open("POST", url, true)
		var datas = JSON.stringify({"firstname":firstname,  "lastname":lastname, "phone":phone, "email":email, "password":password, "gender":gender, "title":title, "position":position});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			if( xml.responseText == 100){
				window.location = base_url + "mainboard/user/" + firstname + "-" + lastname;
			}
						
			
		}
	}
	xml.send(dats);		
}
