
var base_url = "http://localhost/ucms/";

if(window.XMLHttpRequest){
	var xml = new XMLHttpRequest();
	
}
else{
	var xml = new ActiveXObject("Microsoft.XMLHttp");
	
}


function loginstudent()
{
	var matricno = document.getElementById("matric").value;
	var password = document.getElementById("password").value;
	if(matricno == "" || password == ""){
		warning("Incomplete Login details");
		return;
	}
	
	
	var url = base_url + "home/processloginstudent";
	xml.open("POST", url, true)
		var datas = JSON.stringify({ "password":password, "matricno":matricno});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			
			if(xml.responseText == 44){
				warning("Incorrect Login details");
				return;
				
			}
			var checkstatus = xml.responseText.indexOf("registerduser")
			if(checkstatus > 0){
				//console.log(xml.responseText);
				var datas = JSON.parse(xml.responseText);
				window.location = base_url + "mainboard/user/" + datas.names;
			}
			
		}
	}
	xml.send(dats);		
	
	

}

function loginstaff()
{
	
	var staffusername = document.getElementById("staffusername").value;
	var staffpassword = document.getElementById("staffpassword").value;
	if(staffpassword == "" || staffusername == ""){
		warning2("Incomplete Login details");
		return;
	}
	
	
	var url = base_url + "home/processloginstaff";
	xml.open("POST", url, true)
		var datas = JSON.stringify({ "staffpassword":staffpassword, "staffusername":staffusername});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			
			if(xml.responseText == 44){
				warning2("Incorrect Login details");
				return;
				
			}
			var checkstatus = xml.responseText.indexOf("registerduser")
			if(checkstatus > 0){
				
				var datas = JSON.parse(xml.responseText);
				window.location = base_url + "mainboard/user/" + datas.names;
			}
			
		}
	}
	xml.send(dats)
}




function loginadmin()
{
	
	var adminusername = document.getElementById("adminusername").value;
	var adminpassword = document.getElementById("adminpassword").value;
	if(adminpassword == "" ||  adminusername == ""){
		warning3("Incomplete Login details");
		return;
	}
	
	
	var url = base_url + "home/loginadmin";
	xml.open("POST", url, true)
		var datas = JSON.stringify({ "adminusername":adminusername, "adminpassword":adminpassword});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			
			if(xml.responseText == 44){
				warning3("Incorrect Login details");
				return;
				
			}
			
			if(xml.responseText  == 1003){
				
				
				window.location = base_url + "admin/dashboard";
			}
			
		}
	}
	xml.send(dats)
}




function warning3(note)
{
	document.getElementById("error3").innerHTML = note;
}

function warning(note)
{
	document.getElementById("error").innerHTML = note;
}


function warning2(note)
{
	document.getElementById("error2").innerHTML = note;
}

function showadminlogin()
{
	document.getElementById("stafflogin").style.display = "none";
	document.getElementById("studentlogin").style.display = "none";
	document.getElementById("adminlogin").style.display = "block";
}

function showstafflogin()
{
	document.getElementById("stafflogin").style.display = "block";
	document.getElementById("studentlogin").style.display = "none";
	document.getElementById("adminlogin").style.display = "none";
}
function showstudentlogin()
{
	document.getElementById("stafflogin").style.display = "none";
	document.getElementById("studentlogin").style.display = "block";
	document.getElementById("adminlogin").style.display = "none";
}