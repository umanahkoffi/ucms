
var base_url = "http://localhost/ucms/";

if(window.XMLHttpRequest){
	var xml = new XMLHttpRequest();
	
}
else{
	var xml = new ActiveXObject("Microsoft.XMLHttp");
	
}

document.getElementById("com").addEventListener("keyup", function(){
	var val = document.getElementById("com").value;
	var len = val.length;
	var jj = 400 - len;
	document.getElementById("res").innerHTML = jj + "/" + "400"
})


function loginstudent()
{
	var matricno = document.getElementById("matric").value;
	var password = document.getElementById("password").value;
	if(matricno == "" || password == ""){
		warning("Incomplete Login details");
		return;
	}
	
	
	var url = base_url + "home/processloginstudent";
	xml.open("POST", url, true)
		var datas = JSON.stringify({ "password":password, "matricno":matricno});
		var dats = new FormData();
		dats.append("data",datas);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			
			if(xml.responseText == 44){
				warning("Incorrect Login details");
				return;
				
			}
			var checkstatus = xml.responseText.indexOf("registerduser")
			if(checkstatus > 0){
				//console.log(xml.responseText);
				var datas = JSON.parse(xml.responseText);
				window.location = base_url + "mainboard/user/" + datas.names;
			}
			
		}
	}
	xml.send(dats);		
	
	

}

function bringuploader()
{
document.getElementById("uploader").style.display = "block";
}

function closeup()
{
	document.getElementById("uploader").style.display = "none";
}

function doupload()
{
	var upl = document.getElementById("upl");
	var sixe = upl.files[0].type;
	var filesize = upl.files[0].size;
	var arra = new Array("image/jpeg", "image/png", "image/gif");
	if(arra.indexOf(sixe) == -1){
		document.getElementById("error").innerHTML = "File format not supported";
		return;
	}
		
	if(filesize > 1024 * 1024 * 2){
		document.getElementById("error").innerHTML = "File size too large. Size must not be more than 2MB";
		return;
	}
	if((filesize < 1024 * 1024 * 2) && (arra.indexOf(sixe) !== -1)){
		document.getElementById("error").innerHTML = "Uploading in progress......";
	}
		var url = base_url + "mainboard/processupload";
		xml.open("POST", url, true);
		var dats = new FormData();
		dats.append("data", upl.files[0]);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			console.log(xml.responseText);
			var uu = JSON.parse(xml.responseText);
			if(uu.status == "uploaded"){
				document.getElementById("img2").src = base_url + "uploads/" + uu.filename;
				document.getElementById("error").innerHTML  = "";
				document.getElementById("finish").style.display = "block";
			}
			else{
				document.getElementById("error").innerHTML  = "Server error";
			}
		}
	}
	xml.send(dats);	
}

function finish()
{
	var url = base_url + "mainboard/updatepassport";
		xml.open("POST", url, true);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			if(xml.responseText == 100){
				window.location.reload(1);
			}
			else{
				document.getElementById("error").innerHTML  = "Server Error Ocured, Please try again later";
				window.location.reload(1);
			}
		}
	}
	xml.send();	
	
}

document.getElementById("textcomp").addEventListener("keyup",textcomp,true);

function textcomp()
{
	var str = document.getElementById("textcomp").value;
	var url = base_url + "mainboard/searchcomplaincause";
	var datas = JSON.stringify({ "str":str});
		var dats = new FormData();
		dats.append("data",datas);
		xml.open("POST", url, true);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			if(xml.responseText.length == 10){
			}
			else{
			document.getElementById("recievers").innerHTML = xml.responseText;
			}
		
			
		}
	}
	xml.send(dats);	
	
	
}

function addvalue(val)
{
	
	var url = base_url + "mainboard/setvaluetocomplain";
	var datas = JSON.stringify({ "str":val});
		var dats = new FormData();
		dats.append("data",datas);
		xml.open("POST", url, true);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			console.log(xml.responseText);
			document.getElementById("textcomp").value = xml.responseText;
			document.getElementById("recievers").innerHTML = "";
			
		}
	}
	xml.send(dats);	
	
	
}


function logcomplain()
{
	var reason = document.getElementById("com").value;
	var cause = document.getElementById("textcomp").value;
	if(reason == "" ){
		document.getElementById("recievers").innerHTML = "Type a complain";	
		setTimeout(function(){ document.getElementById("recievers").innerHTML = "";},3000)
		return;
	}
	if(cause == "" ){
		document.getElementById("recievers").innerHTML = "Provide those responsible for the complain";
		setTimeout(function(){ document.getElementById("recievers").innerHTML = "";},3000)	
		return;
	}
	
	 document.getElementById("com").value = "";
	  document.getElementById("textcomp").value = "";
	  document.getElementById("recievers").innerHTML = "";
	  
	var url = base_url + "mainboard/logcomplain";
	var datas = JSON.stringify({ "reason":reason, "cause":cause});
		var dats = new FormData();
		dats.append("data",datas);
		
		
		xml.open("POST", url, true);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			document.getElementById("form9").innerHTML  = xml.responseText;
			
		}
	}
	xml.send(dats);	
	
	
}


window.onload = loadpreviouscomplaints()

function loadpreviouscomplaints()
{
	setTimeout(function(){
	var url = base_url + "mainboard/loadpreviouscomplaints";
		xml.open("POST", url, true);
		xml.onreadystatechange = function(){
		if(xml.readyState == 4 && xml.status == 200){
			
				document.getElementById("form9").innerHTML  = xml.responseText;
				
			
		}
	}
	xml.send();	
	}, 2000)
}